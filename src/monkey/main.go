package main

import(
     "fmt"
     "os"
     "net/http"
     "log"
     "encoding/json"
     "strings"
     "regexp"
     "html/template"
)


var tmpl *template.Template

type Response struct {
  Result string `json:"result"`
  Message string `json:"message"`
}

type htmldata struct {
  Say string
  Result int
}

func init() {
    tmpl = template.Must(template.ParseGlob("templates/*.gohtml"))
}

func HtmlSay (w http.ResponseWriter, r *http.Request ) {

  if r.Method != "GET" {
    fmt.Fprintf(w,"Unable to handle none GET requests\n")
    fmt.Fprintf(os.Stderr, "Unable to handle none GET request\n")
    return
  }

  fullquery := r.URL.RawQuery
  fullquery = strings.ReplaceAll(fullquery,",","&")

  splitquery := strings.Split(fullquery, "&")
  resultFlag := 0

  var data htmldata

  for i := 0 ; i < len(splitquery); i++ {
    sayReg := regexp.MustCompile(`(say){1}`)
    if sayReg.MatchString(splitquery[i]) {

       saySplit := strings.Split(splitquery[i], "=")
       if saySplit[1] != "" {

          var msg strings.Builder
          msg.WriteString(saySplit[1])

          resultFlag = 1

          data.Say = msg.String()
          data.Result = resultFlag
       } else if resultFlag != 1 {
            data.Say = ""
            data.Result = resultFlag
       }
    }
  }
  tmpl.ExecuteTemplate(w, "response.gohtml", data)
}

func Monkey(w http.ResponseWriter, r *http.Request) {

  ContentType := r.Header.Get("Content-type")

  if r.Method != "GET" {
    fmt.Fprintf(w, "Unable to handle none GET request")
    fmt.Fprintf(os.Stderr, "Unable to handle none GET request")

    return
  }

  if ContentType == "application/json" {

     fmt.Fprintf(os.Stdout, "received an application/json cotent type\n")

     var response Response

     fullquery := r.URL.RawQuery
     fullquery = strings.ReplaceAll(fullquery,",","&")

     splitquery := strings.Split(fullquery, "&")

     resultFlag := 0
     for i := 0 ; i < len(splitquery); i++ {
     sayReg := regexp.MustCompile(`(say){1}`)
     if sayReg.MatchString(splitquery[i]) {

        saySplit := strings.Split(splitquery[i], "=")
        response.Result = "Success"
        var msg strings.Builder
        msg.WriteString("Monkey says: ")
        msg.WriteString(saySplit[1])
        response.Message = msg.String()
        resultFlag = 1

        } else if resultFlag != 1 {
          response.Result = "Failure"
          response.Message = "No says variable in the Query"
        }
      }

      sendJson , err := json.MarshalIndent(response, "", "  ")

      if err != nil {
          fmt.Fprintf(os.Stderr, "Unable to Marshal the Request\n")
          http.Error(w, "Unable to Marshal the Request", http.StatusBadRequest)
      }

      if _ , werr := w.Write(sendJson); werr != nil {
        fmt.Fprintf(os.Stderr, "Unable to Write the Response\n")
        http.Error(w, "Unable to Write Response\n", http.StatusBadRequest)
      }

  } else {

    fmt.Fprintf(os.Stdout, "ContentType none application/json\n")
    tmpl.ExecuteTemplate(w, "index.gohtml", nil)
  }
}

func main() {
  port , found := os.LookupEnv("PORT")

  if !found {
    port = "8080"
  }

  http.HandleFunc("/", Monkey)
  http.HandleFunc("/monkeysay", HtmlSay)
  http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir("images"))))

  log.Printf("Starting HTTP Service on port %v", port)
  log.Fatal(http.ListenAndServe(":"+port, nil))
}
